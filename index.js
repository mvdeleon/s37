const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();



mongoose.connect("mongodb+srv://admin123:admin123@project0.sid71ld.mongodb.net/s37-41?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);



mongoose.connection.once('open',()=>console.log("Now connected to MongoDB Atlas."))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT||4000}`)
});